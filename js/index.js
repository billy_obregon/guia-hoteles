$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
  });
  $('.carousel').carousel({
    interval: 2000
  });
  $('#contactoReserva').on('show.bs.modal', function (e) {
    console.log('El modal se esta mostrando');
    
    $('#ContactoModall').removeClass('btn-primary');
    $('#ContactoModall').addClass('btn-outline-success');
    $('#ContactoModall').prop('disabled', true);
  });
  $('#contactoReserva').on('shown.bs.modal', function (e) {
    console.log('El modal se mostró');
  });
  $('#contactoReserva').on('hide.bs.modal', function (e) {
    console.log('El modal se oculta');
  });
  $('#contactoReserva').on('hidden.bs.modal', function (e) {
    console.log('El modal se ocultó');
    
    $('#ContactoModall').removeClass('btn-outline-success');
    $('#ContactoModall').addClass('btn-primary');
    $('#ContactoModall').prop('disabled', false);



  });